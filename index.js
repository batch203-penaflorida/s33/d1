// SECTION JavaScript Synchronous vs Asynchronous

/* 
 JavaScript is by default synchronous meaning that it only executes one statement at a time.
 
*/

console.log("Hello World!");
// conosole.log("Hello Again");
console.log("Good Bye");
// Code blocking - waiting for the specific sattement to finish before executing the next statement
// for (let i = 0; i <= 1500; i++) {
//   console.log(i);
// }

console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, whiile time consuming code is running in the background.

// [SECTION] Getting all posts

// The fetch API that allows us to asynchronously request for a resource (data).

// Fetch method is used in JavaScript to request to the server and load information on the webpages.

// Syntax:
/* 
   fetch ("apiURL")
   A promise is an object that represents the evantual completion (or failure) of an asynchronous function and it's resulting value.
   A promise may be in one of 3 possible states: fullfilled, rejected, or pending.

   pending: intial states, neither fullfiled nor rejected response.

   fullfiled: operation was completed
   
   rejected: operation failed.

   syntax:
   fetch("apiURL")
   .then((res)=>{
      console.log(response.status)
   })
   .catch((err)=>{
      
   });
*/

fetch("https://jsonplaceholder.typicode.com/posts");
// The .then() method captures the response.
//   .then((response) => console.log(response.status))
//   .catch((err) => console.log(err));
// json() method will convert JSON format to JavaScript Object.

//   .then((res) => res.json())
//   // we can now manipulate or use the converted response in our code.
//   //   We can use array methods to display each post title.
//   .then((json) => json.forEach((post) => console.log(post.title)));

// The "async" and "await" keyword to achieve asynchronous code.

async function fetchData() {
  let result = await fetch("https://jsonplaceholder.typicode.com/posts");
  // it returned the "Response" object.

  console.log(result);
  let json = await result.json();
  console.log(json);
}

fetchData();
// Getting a specific post
// Retrieves a specific post following the REST API retrieve, /post/:id, GET

/* 
   :id is a wildcard where you can put any value
*/

fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then((response) => response.json())
  .then((json) => console.log(json));

//   Creating a post

/* 
   Syntax:
      fetch("apiURL", options)
      .then(response =>{})
      .then(response => {})
      .catch()
*/
fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "New Post",
    body: "Hello World!",
    userId: 1,
  }),
}).then((response) => response.json().then((json) => console.log(json)));

// [SECTION] Updating a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Updated Post",
    body: "Hello Again!",
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

//   [SECTION] Updating a post using PATCH method
// PUT vs PATCH

// PATCH is used to update a single/several properties
// PUT is used to update the whole document/object

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
  },
  body: JSON.stringify({
    title: "Corrected post title",
  }),
})
  .then((response) => response.json())
  .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
})
  .then((response) => response.json())
  .then((json) => console.log(json));
